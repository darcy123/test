
puts"Commands:"
puts"s3123456_p2.rb -xml [filename] # Load a XML file"
puts"s3123456_p2.rb help [COMMAND] # Describe available commands or one specific command"


require 'nokogiri'
require 'json'


class Converter
    

attr_reader :file, :records
    
def initialize file
    @file = file
    @records = []

    end
    
    
    def read
        @file.xpath("//record").each do |record|
            @records << {:id => record.xpath('id').text, 
                    :first_name => record.xpath('first_name').text, 
                    :last_name => record.xpath('last_name').text, 
                    :email => record.xpath('email').text, 
                    :gender => record.xpath('gender').text, 
                    :ip_address => record.xpath('ip_address').text,
                    :send_date => record.xpath('send_date').text,
                    :email_body => record.xpath('email_body').text,
                    :email_title => record.xpath('email_title').text}
                
                end
            self
        end
         
        def ip_searcher(ip)
            @records.select {|k| k[:ip_address] == ip.to_s}
            
        end
        
        def name_search(n)

                @records.select { |i| (i[:first_name] + i[:last_name]).include? n.to_s}

        end
   
    end 
    
    file =Dir['*xml'].first
    document = Nokogiri::XML(File.open("#{file}"))
xml = Converter.new(document)
    
    x = ARGV[0]
    case x
       
        when '-xml'
        filename = ARGV[1]
        document = Nokogiri::(File.open("#{filename}"))
    when 'help'
    
        puts " ↓ ↓ ↓ All available commands  ↓ ↓ ↓"
puts "WARNING>>> COMMANDS ARE CASE SENSITIVE list WILL NOT WORK List WILL WORK <<<WARNING"
        puts "help: Displays all available commands"
        puts "List: lists all extracted xml records "
        puts "List --ip: is an extension of  'List' this command will allow to search and display a record by ip address"
        puts "List --name: is an extension of  'List' this command will allow to search and display a record by name"
        
        when 'list'
        if ARGV[1].eql?nil
            puts "Please input Commands
for more informtion on availible commands use the help command  help  "
            
        elsif ARGV[1].eql? '--name'
            personname = ARGV[2]
            name_print = xml.read.name_search("#{personname}")
            puts JSON.pretty_generate(name_print)
        elsif ARGV[1] .eql? '--ip'
            ip_add = ARGV[2]
            ip_print = xml.read.ip_searcher("#{ip_add}")
            puts JSON.pretty_generate(ip_print)
            
        else
            puts "the inputted command is not a valid extension of 'List'"
        end
    else
        puts "This command does nto exist, for more information on commands use 'help'"
    end
        
